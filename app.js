var date = new Date();
var dates = [];
var ONE_DAY = 24*60*60*1000;

var tableTemplate = function(app) {
    var el = document.createElement('table');
    el.id = 'curency';
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('Date'));
    el.appendChild(th)
    for(var k in CURRENCY){
        th = document.createElement('th');
        th.appendChild(document.createTextNode(k));
        th.id = k;
        el.appendChild(th);
        th.addEventListener('click', function(){
            app.getCurrencyRange(this.id);
        });
    }

    return el;
}

var rowTempalte = function(result){
    var tr = document.createElement('tr');
    var td = document.createElement('td');
    td.appendChild(document.createTextNode(result.date))
    tr.appendChild(td);


    for(var k in CURRENCY){
        td = document.createElement('td');
        td.appendChild(document.createTextNode(result.currencies[k]))
        tr.appendChild(td);
    }

    return tr
}

var rangeTemplate = function(data, currency){
    var el = document.createElement('table');
    el.id = 'range';
    var str = '<tr>' +
            '<th>Date</th>'+
            '<th>'+currency+'</th>'+
            '</tr>';
    el.innerHTML = str;

    for(var date in data){
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(date));
        tr.appendChild(td);

        td = document.createElement('td');
        td.appendChild(document.createTextNode(data[date]));
        tr.appendChild(td);

        el.appendChild(tr);
    }
    return el;
}

for(var i = 0; i < 5; i++){
    dates.push(date - ONE_DAY*i);
}

var App = function(){
    var self = this;
    this.api = new Api();
    dates.forEach(function(date){
        self.api.subscribe(date);
    });
    this.api.complete(function(){
        self.appendResults(self.api.results);
    });

    this.api.getCurrency();
}

App.prototype.appendResults = function(results){
    var self = this;
    var t = tableTemplate(this);
    results.forEach(function(result){
        t.appendChild(rowTempalte(result));
    })
    document.body.appendChild(t);
}


App.prototype.getCurrencyRange = function(currency){
    this.api.getRange(new Date - ONE_DAY*10, +new Date, currency, function(result){
        document.body.appendChild(rangeTemplate(result.result, result.currency));
    });
}
