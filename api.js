var CURRENCY = {
    'BYR': 'R01090',
    'USD': 'R01235',
    'EUR': 'R01239'
};

var Result = function(date, data){
    this.date = new Date(date);
    this.currencies = {};
    this.data = this.parse(data);
    this.findCurrencies();
}

Result.prototype.parse = function(data){
    return (new DOMParser()).parseFromString(data, "text/xml");
}

Result.prototype.findCurrencies = function(){
    var self = this;
    for(var k in CURRENCY){
        var id = CURRENCY[k];
        this.currencies[k] = this.data.querySelector('[ID="'+id+'"] Value').innerHTML;
    }
}

var RangeResult = function(currency, data){
    this.currency = currency;
    this.data = Result.prototype.parse.call(this, data);
    var dates = this.data.querySelectorAll(['Record']);
    var values = this.data.querySelectorAll(['[Date] Value']);
    this.result = {};
    for(var i =0 ;i<dates.length; i++){
        var d = dates[i].getAttribute('Date');
        this.result[d] = values[i].innerHTML;
    }
}

var Api = function(){
    this.subscriptions = [];
    this.results = [];
}

Api.prototype.complete = function(cb){
    this._on_complete = cb;
}

Api.prototype.getCurrency = function(){
    var self = this;
    this.subscriptions.forEach(function(date){
        var xhr = new XMLHttpRequest
        xhr.open('GET', '/api/all?date='+date);
        xhr.addEventListener('load', function(resp){
            self.results.push(new Result(date, xhr.responseText));
            if(self.results.length == 5){
                if(self._on_complete){
                    self._on_complete();
                }
            }
        })
        xhr.send();
    })
}

Api.prototype.getRange = function(date1, date2, currency, cb){
    var self = this;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/api/currency?date1='+date1 + '&date2='
                                          +date2 + '&currency='
                                          +CURRENCY[currency]);

    xhr.addEventListener('load', function(resp){
        cb(new RangeResult(currency, xhr.responseText));
    })
    xhr.send();
}

Api.prototype.subscribe = function(date){
    this.subscriptions.push(date);
}
