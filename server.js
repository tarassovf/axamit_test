var http = require('http');
var https = require('https');
var fs = require('fs');

var Iconv = require('iconv').Iconv;

var APP_PATH = __dirname;
var port = 8081;

function addZero(val){
    return val < 10 ? '0'+val : val;
}

function strDate(date){
    return '' + addZero(date.getDate()) + '/' +
      addZero(date.getMonth()) + '/' +
      date.getFullYear();
}

var readParams = function(url){
    var split = url.split('?');
    var params = {};
    if(split[1]){
        var splitParams = split[1].split('&');
        for(var i = 0; i < splitParams.length; i++){
            var p = splitParams[i].split('=');
            params[p[0]] = p[1];
        }
    }
    return params;
}

var server = http.createServer(function(request, response){
    var file = APP_PATH + request.url;
    if(request.url == '/'){
        file += 'index.html';
    }
    if(fs.existsSync(file)){
        response.end(fs.readFileSync(file));
    } else if(request.url.match(/^\/api\/all/)){

        var params = readParams(request.url);
        var date = new Date(parseInt(params['date']));

        var req = http.request({
            hostname: 'www.cbr.ru',
            port: 80,
            path: '/scripts/XML_daily.asp?date_req='+strDate(date),
            method: 'GET',
            encoding: 'binary'
        },
        function(res){
            var body = new Buffer('', 'binary');
            res.on('data', function(chunk) {
                body += chunk;
            });
            res.on('end', function() {
                var conv = new Iconv('CP1251', 'utf8');
                response.end(conv.convert(body));
            });
        }).on('error', function(e){
            console.log(e.message)
        })
        req.end();
    } else if(request.url.match(/^\/api\/currency/)){
        var params = readParams(request.url);

        var date1 = new Date(parseInt(params['date1']));
        var date2 = new Date(parseInt(params['date2']));

        var req = http.request({
            hostname: 'www.cbr.ru',
            port: 80,
            path: '/scripts/XML_dynamic.asp?date_req1='+ strDate(date1)+
                                        '&date_req2='  + strDate(date2)+
                                        '&VAL_NM_RQ='  + params['currency'],
            method: 'GET',
            encoding: 'binary'
        },
        function(res){
            var body = new Buffer('', 'binary');
            res.on('data', function(chunk) {
                body += chunk;
            });
            res.on('end', function() {
                var conv = new Iconv('CP1251', 'utf8');
                response.end(conv.convert(body));
            });
        }).on('error', function(e){
            console.log(e.message)
        })
        req.end();
    } else {
        response.end('404 not sfound');
    }
})

server.listen(port);
